@extends('layouts.app')

@section('content')
    <div class="container">
           <div class="row">
            <div class="col-md-8"></div>
                <div class="card">
                    <div class="card-heading">Edit Post</div>
                    <div class="card-body">
                        <form class="" action="{{ route('post.update', $post) }}" method="post">
                            <!-- agar tidak error -->
                            {{ csrf_field() }}
                            {{ method_field('PATCH')}}
                            <div class="form-group">
                                <label for="">Title</label>
                                    <input type="text" class="form-control" name="title" value="" placeholder="Insert title">
                            </div>

                            <div class="form-group">
                                <label for="">Category</label>
                                  <select name="category_id" id="" class="form-control" >
                                      <!-- Perulangan foreach untuk menampilkan
                                      semua isi array atau data dengan perintah yang lebih singkat
                                      daripada menggunakan perulangan for -->
                                      @foreach ($categories as $category)
                                          <option value="{{ $category->id}}"> {{ $category->name }}</option>
                                      @endforeach
                                  </select>
                            </div>

                            <div class="form-group">
                                <label for="">Content</label>
                                    <textarea name="content" rows="5" class="form-control" value="" placeholder="Insert content"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="save" >
                            </div>
                        </form>
                    </div>
                  </div>
          </div>
    </div>
@endsection
