<!-- penanda bahwa kita menggunakan layouts app.blade.php sebagai layout website -->
@extends('layouts.app')

<!--  menentukan section -->
<!-- isi bagian konten -->
<!-- cara penulisan isi section yang panjang -->
@section('content')
  <div class="container">
        <form class="" action="{{ route('post.store') }}" method="post">
          <!-- agar tidak error -->
          {{ csrf_field() }}
            <div class="form-group">
              <label for="">Title</label>
              <input type="text" class="form-control" name="title" value="" placeholder="Insert title">
            </div>

            <div class="form-group">
              <label for="">Category</label>
              <select name="category_id" id="" class="form-control" >
                <!-- Perulangan foreach untuk menampilkan
                semua isi array atau data dengan perintah yang lebih singkat
                daripada menggunakan perulangan for -->
                  @foreach ($categories as $category)
                      <option value="{{ $category->id}}"> {{ $category->name }}</option>
                  @endforeach
              </select>
            </div>

            <div class="form-group">
              <label for="">Content</label>
              <textarea name="content" rows="5" class="form-control" value="" placeholder="Insert content"></textarea>
            </div>

            <div class="form-group">
              <input type="submit" class="btn btn-primary" value="save" >
            </div>
        </form>
  </div>

<!-- mengakhiri section -->
@endsection
