<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;

class PostController extends Controller
{
  public function index()
  {
    $posts = Post::all();
    return view('post.index', compact('posts'));
  }

  public function create()
  {
    /*
    mengambil semua data dari model category terus
    di simpan ke variabel baru
    */
    $categories = Category::all();

    /*
    menampilkan data yang ingin di buat dari data di database untuk proses
    membuat sebuah post
    */
    return view('post.create', compact('categories'));
  }

  public function store()
  {
    //semua data yang diperlukan dan akan disimpan ke database
    Post::create([
        'title'       => request('title'),
        'slug'        => str_slug(request('title')),
        'content'     => request('content'),
        'category_id' => request('category_id')
    ]);
    //setelah proses menyimpan data ke database selesai kembali page home
    return redirect()->route('post.index');
  }

  public function edit($id)
  {
      $post = Post::find($id);
      $categories = Category::all();

      return view('post.edit', compact('post', 'categories'));
  }

  public function update($id)
  {
      $post = Post::find($id);

      $post->update([
          'title'      => request('title'),
          'category_id'=> request('category_id'),
          'content'    => request('content'),

      ]);

      return redirect()->route('post.index');
  }
}
