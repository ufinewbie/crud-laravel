<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /*
    berguna untuk mendaftarkan atribut (nama kolom) yang bisa kita isi
    ketika melakukan insert atau update ke database
    */
    protected $fillable = ['title', 'content', 'category_id', 'slug'];
}
